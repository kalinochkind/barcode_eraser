#!/usr/bin/env python3

from PIL import Image, ImageDraw
import numpy as np
import sys
import os
import shutil
import subprocess
import concurrent.futures
import hashlib

def string_hash(s):
    return hashlib.sha1(s.encode('utf-8')).hexdigest()


if len(sys.argv) < 2 or not os.path.isdir(sys.argv[1]):
    print('Перетащите папку на значок этой программы или запустите ее в командной строке, передав путь к папке')
    input()
    sys.exit(1)

PDF_DIR = sys.argv[1]
OUT_DIR = PDF_DIR.rstrip('/\\') + '_output'
if not os.path.isdir(OUT_DIR):
    os.mkdir(OUT_DIR)
if not os.path.isdir('jpgs'):
    os.mkdir('jpgs')


def has_barcode(filename, offset=0):
    img = Image.open(filename)
    offset = int(offset * img.height / 9)
    bw = img.convert('L').crop((int(img.width * 0.637), int(img.height / 117) + offset, int(img.width * 0.925), int(img.height / 10) + offset))

    arr = np.array(bw)
    arr = (arr > 200)
    arr = (((arr[:-1,:-1] ^ arr[:-1,1:])) & ((arr[1:,:-1] ^ arr[1:,1:]))) & (~(arr[:-1,:-1] ^ arr[1:,:-1]))
    arr = arr[:,:-2] | arr[:,1:-1] | arr[:,2:]
    means = arr.mean(axis=1)
    s = np.cumsum(means)
    s = s[20:] - s[:-20]
    return np.max(s) > 5


def get_density(filename):
    img = Image.open(filename)
    return '200' if img.height > 2000 else '150'


def draw_rect(filename, offset=0):
    img = Image.open(filename)
    offset = int(offset * img.height / 9)
    draw = ImageDraw.Draw(img)
    draw.rectangle([int(img.width * 0.655), offset, img.width, int(img.height / 8.66) + offset], fill='white')
    img.save(filename, 'jpeg', quality=90)


def process_pdf(filename, remove_pages=0):
    print('Processing', filename, flush=True)
    img_dir = os.path.join('jpgs', string_hash(filename))
    try:
        os.mkdir(img_dir)
    except FileExistsError:
        shutil.rmtree(img_dir)
        os.mkdir(img_dir)
    subprocess.call(['pdfimages', '-j', os.path.join(PDF_DIR, filename), os.path.join('jpgs', string_hash(filename), 'img')])
    changed = False
    detected = False
    density = None
    for i, img in enumerate(sorted(os.listdir(img_dir))):
        img_file = os.path.join(img_dir, img)
        if i < remove_pages:
            if not density:
                density = get_density(img_file)
            os.remove(img_file)
            changed = True
            continue
        has = has_barcode(img_file)
        if has:
            if not detected and i > remove_pages:
                print('SOMETHING BAD', filename, flush=True)
            draw_rect(img_file)
            changed = True
            detected = True
            if not density:
                density = get_density(img_file)
        elif i == remove_pages:
            has = has_barcode(img_file, 1)
            if has:
                print('Unusual position', filename)
                draw_rect(img_file, 1)
                changed = True
                detected = True
                if not density:
                    density = get_density(img_file)
    if changed:
        if detected:
            print('Detected', filename, density, flush=True)
        subprocess.call(['convert', '-density', density, os.path.join('jpgs', string_hash(filename), '*.*'), os.path.join(OUT_DIR, filename)])
    else:
        shutil.copy(os.path.join(PDF_DIR, filename), os.path.join(OUT_DIR, filename))
    shutil.rmtree(img_dir)

def main(remove_pages=0):
    with concurrent.futures.ThreadPoolExecutor(min(os.cpu_count() or 4, 4)) as executor:
        for filename in sorted(os.listdir(PDF_DIR)):
            if os.path.exists(os.path.join(OUT_DIR, filename)):
                continue
            executor.submit(process_pdf, filename, remove_pages)
            #process_pdf(filename, remove_pages)
    print()
    print('Обработка завершена')
    input()

if __name__ == '__main__':
    main()
