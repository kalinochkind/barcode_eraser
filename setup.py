import sys
from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need fine tuning.
build_exe_options = {"excludes": ["tkinter"]}

# GUI applications require a different base on Windows (the default is for a
# console application).
base = None

setup(  name = "barcode_eraser",
        version = "0.1",
        description = "Barcode eraser",
        options = {"build_exe": build_exe_options},
        executables = [Executable("erase.py", base=base), Executable("erase_with_form.py", base=base)])
